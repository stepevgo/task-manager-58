package ru.t1.stepanishchev.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.stepanishchev.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.stepanishchev.tm.api.service.IConnectionService;
import ru.t1.stepanishchev.tm.api.service.dto.IProjectDTOService;
import ru.t1.stepanishchev.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.stepanishchev.tm.api.service.dto.ITaskDTOService;
import ru.t1.stepanishchev.tm.dto.model.ProjectDTO;
import ru.t1.stepanishchev.tm.dto.model.TaskDTO;
import ru.t1.stepanishchev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.stepanishchev.tm.exception.entity.TaskNotFoundException;
import ru.t1.stepanishchev.tm.exception.field.ProjectIdEmptyException;
import ru.t1.stepanishchev.tm.exception.field.TaskIdEmptyException;
import ru.t1.stepanishchev.tm.exception.field.UserIdEmptyException;
import ru.t1.stepanishchev.tm.repository.dto.TaskDTORepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectTaskDTOService extends AbstractUserOwnedDTOService implements IProjectTaskDTOService {

    @NotNull
    @Autowired
    public IProjectDTOService projectService;

    @NotNull
    @Autowired
    public ITaskDTOService taskService;

    @Override
    public @NotNull EntityManager getEntityManager() {
        return context.getBean(EntityManager.class);
    }

    @NotNull
    public TaskDTORepository getRepository() {
        return context.getBean(TaskDTORepository.class);
    }

    @Override
    @SneakyThrows
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final ProjectDTO project = projectService.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        @Nullable TaskDTO task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        @NotNull final ITaskDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final ProjectDTO project = projectService.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        @Nullable TaskDTO task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        @NotNull final ITaskDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public ProjectDTO removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable final ProjectDTO project = projectService.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final List<TaskDTO> tasks = taskService.findAllByProjectId(userId, projectId);
        for (@NotNull final TaskDTO task : tasks) taskService.removeOneById(userId, task.getId());
        projectService.removeOneById(userId, projectId);
        return project;
    }

}