package ru.t1.stepanishchev.tm.service.dto;

import jdk.nashorn.internal.objects.annotations.Getter;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.stepanishchev.tm.api.repository.dto.IAbstractDTORepository;
import ru.t1.stepanishchev.tm.api.service.dto.IAbstractDTOService;
import ru.t1.stepanishchev.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.Collection;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDTOService<M extends AbstractModelDTO, R extends IAbstractDTORepository<M>>
        implements IAbstractDTOService<M> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @NotNull
    protected abstract IAbstractDTORepository<M> getRepository();

    @Override
    public void add(@NotNull final M model) {
        @NotNull final IAbstractDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addAll(@NotNull final Collection<M> models) {
        @NotNull final IAbstractDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.addAll(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@NotNull final M model) {
        @NotNull final IAbstractDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@NotNull final M model) {
        @NotNull final IAbstractDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}