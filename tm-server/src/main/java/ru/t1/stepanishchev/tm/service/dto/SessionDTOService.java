package ru.t1.stepanishchev.tm.service.dto;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.stepanishchev.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.stepanishchev.tm.api.service.IConnectionService;
import ru.t1.stepanishchev.tm.api.service.dto.ISessionDTOService;
import ru.t1.stepanishchev.tm.dto.model.SessionDTO;
import ru.t1.stepanishchev.tm.exception.entity.SessionNotFoundException;
import ru.t1.stepanishchev.tm.exception.field.IdEmptyException;
import ru.t1.stepanishchev.tm.exception.field.UserIdEmptyException;
import ru.t1.stepanishchev.tm.repository.dto.SessionDTORepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

@Service
@NoArgsConstructor
public class SessionDTOService extends AbstractUserOwnedDTOService<SessionDTO, ISessionDTORepository>
        implements ISessionDTOService {

    @NotNull
    @Override
    public SessionDTORepository getRepository() {
        return context.getBean(SessionDTORepository.class);
    }

    @Override
    public @NotNull EntityManager getEntityManager() {
        return context.getBean(EntityManager.class);
    }

    @NotNull
    public SessionDTO create(@Nullable final SessionDTO session) {
        if (session == null) throw new SessionNotFoundException();
        @NotNull final ISessionDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(session);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return session;
    }

    public Boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneById(id) != null;
    }

    @Nullable
    public SessionDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final ISessionDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final SessionDTO session = repository.findOneById(id);
            return session;
        }
        finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ISessionDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    public SessionDTO removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable SessionDTO session = findOneById(id);
        if (session == null) throw new SessionNotFoundException();
        @NotNull final ISessionDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.remove(session);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return session;
    }

}