package ru.t1.stepanishchev.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.stepanishchev.tm.api.repository.model.IAbstractRepository;
import ru.t1.stepanishchev.tm.api.service.IConnectionService;
import ru.t1.stepanishchev.tm.api.service.model.IAbstractService;
import ru.t1.stepanishchev.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.Collection;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractService<M extends AbstractModel, R extends IAbstractRepository<M>>
        implements IAbstractService<M> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    protected EntityManager getEntityManager() {
        return context.getBean(EntityManager.class);
    }

    @NotNull
    protected abstract IAbstractRepository<M> getRepository();

    @Override
    public void add(@NotNull final M model) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IAbstractRepository<M> repository = getRepository();
        try {
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addAll(@NotNull final Collection<M> models) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IAbstractRepository<M> repository = getRepository();
        try {
            entityManager.getTransaction().begin();
            repository.addAll(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@NotNull final M model) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IAbstractRepository<M> repository = getRepository();
        try {
            entityManager.getTransaction().begin();
            repository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@NotNull final M model) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IAbstractRepository<M> repository = getRepository();
        try {
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}