package ru.t1.stepanishchev.tm.exception.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException(@NotNull final String argument) {
        super("Error! Argument '" + argument + "' is not supported.");
    }

}