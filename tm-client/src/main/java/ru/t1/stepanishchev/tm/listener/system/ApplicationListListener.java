package ru.t1.stepanishchev.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.api.IListener;
import ru.t1.stepanishchev.tm.event.ConsoleEvent;
import ru.t1.stepanishchev.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class ApplicationListListener extends AbstractSystemListener {

    @NotNull
    private final String NAME = "commands";

    @NotNull
    private final String ARGUMENT = "-cmd";

    @NotNull
    private final String DESCRIPTION = "Show command list.";

    @Override
    @EventListener(condition = "@applicationListListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[COMMANDS]");
        for (@Nullable final AbstractListener abstractListener: listeners) {
            if (abstractListener == null) continue;
            @Nullable final String name = abstractListener.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name.toString());
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}