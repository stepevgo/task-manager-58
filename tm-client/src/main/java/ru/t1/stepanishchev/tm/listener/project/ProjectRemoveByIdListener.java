package ru.t1.stepanishchev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.dto.request.ProjectRemoveByIdRequest;
import ru.t1.stepanishchev.tm.event.ConsoleEvent;
import ru.t1.stepanishchev.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByIdListener extends AbstractProjectListener {

    @NotNull
    private final String NAME = "project-remove-by-id";

    @NotNull
    private final String DESCRIPTION = "Remove project by id.";

    @Override
    @EventListener(condition = "@projectRemoveByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(getToken(), id));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}